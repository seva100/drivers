# -*- coding: utf-8 -*-
"""
Created on Sat Mar 14 20:11:06 2015

@author: Artem Sevastopolsky
"""

REPORT_NAME = "16-03-2015 01-05-24(logistic3).csv"
MAX_DRIVERS = 3612
MAX_TRIPS = 200

from operator import itemgetter
import numpy as np
import csvHandle

def rerank(answer):
    # answer is list of computed probabilities
    # returns np.array of reranked probabilities
    pairs = enumerate(answer)
    pairs = sorted(pairs, key=itemgetter(1), reverse=True)
    newAnswer = np.empty_like(answer, dtype="float")
    for i in xrange(len(pairs)):
        newAnswer[pairs[i][0]] = 1 - i / 200.0
    return newAnswer

if __name__ == "__main__":
    DRIVERS_LIST = np.array(csvHandle.getAvailableDrivers())
    
    inFile, inReader = csvHandle.openReadStream("report", REPORT_NAME)
    inReader.next()     # skipping "driver_trip,prob"
    outFile, outWriter = csvHandle.openWriteStream("report", "logistic3-reranked")
    ALL_DATA_VOL = 547200
    answer = np.empty(ALL_DATA_VOL, dtype="float")
    i = 0
    while True:
        try:
            line = inReader.next()
        except StopIteration:
            break
        else:
            answer[i] = float(line[1])
            i += 1
    answer = answer.reshape((len(DRIVERS_LIST), MAX_TRIPS))
    newAnswer = np.empty_like(answer)
    for i in xrange(len(DRIVERS_LIST)):
        driverNo = DRIVERS_LIST[i] - 1
        newAnswer[i, :] = rerank(answer[i, :])
        print("data for driver {} reranked".format(driverNo))
    #print(answer)
    #print(newAnswer)
    for i in xrange(len(DRIVERS_LIST)):
        for tripNo in xrange(MAX_TRIPS):
            driverNo = DRIVERS_LIST[i]
            outWriter.writerow(("{}_{}".format(driverNo, tripNo + 1), 
                                newAnswer[i, tripNo]))
        print("data for driver {} written".format(driverNo))
    
    csvHandle.closeStream(outFile)
    inFile.close()