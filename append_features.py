# -*- coding: utf-8 -*-
"""
Created on Fri Mar 13 08:53:26 2015

@author: Artem S
"""

FEATURES_OUTPUT_DIR = r"/home/artem/Programming/Kaggle drivers/features7q"
MAX_DRIVERS = 3612

import csvHandle
import os
import numpy as np

if __name__ == "__main__":
    DRIVERS_LIST = np.array(csvHandle.getAvailableDrivers())
    DRIVERS_LIST = DRIVERS_LIST[DRIVERS_LIST <= MAX_DRIVERS]
    DRIVERS_LIST = DRIVERS_LIST[DRIVERS_LIST >= 3496]
    for driverNo in DRIVERS_LIST:
        features = csvHandle.readFirstFeatures(driverNo)
        n = features.shape[1]
        features = np.resize(features, (features.shape[0], n * (1 + (n + 1) / 2)))
        newIdx = n
        for i in xrange(n):
            for j in xrange(i, n):
                features[:, newIdx] = features[:, i] * features[:, j]
                newIdx += 1
        newFile, newWriter = csvHandle.openWriteStream("none", 
           os.path.join(FEATURES_OUTPUT_DIR, "{}.csv".format(driverNo)))
        for lineNo in xrange(features.shape[0]):
            newWriter.writerow(features[lineNo, :])
        newFile.close()
        print "features for driver {} stored".format(driverNo)
           