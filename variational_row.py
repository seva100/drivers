# -*- coding: utf-8 -*-
"""

Created on Sun Mar 15 17:31:30 2015

@author: Artem Sevastopolsky
"""

REPORT_NAME = "15-03-2015 20-45-20(logistic3-reranked).csv"
MAX_DRIVERS = 3612
MAX_TRIPS = 200
DRIVER_NO = 1

from operator import itemgetter
import numpy as np
import csvHandle

def variationalRow(x):
    pairs = enumerate(x)
    pairs = sorted(pairs, key=itemgetter(1))
    return pairs

if __name__ == "__main__":
    answer = csvHandle.reportForDriver(REPORT_NAME, DRIVER_NO)[1:]
        # skipping fictive 0 element
    _varRow = variationalRow(answer)
    vals = map(itemgetter(0), _varRow)
    vals = np.array(vals) + 1
    print(vals)
