# Project README #

This is the solution of Drivers Telematics Analysis competition on Kaggle (https://www.kaggle.com/c/axa-driver-telematics-analysis).
Solution achieves 0.84 AUC-ROC score on the platform.

At the time of coding, author was not skilled enough to use Numpy/Scipy and Scikit-learn libraries, so the code does not make a proper use of these tools.

Expected order of scripts execution:

1. generate_features.py

2. main_logistic.py

3. reranking.py

Data set of the competition can be downloaded from the competition page: https://www.kaggle.com/c/axa-driver-telematics-analysis/data