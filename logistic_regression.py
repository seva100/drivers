# -*- coding: utf-8 -*-
"""

Created on Thu Mar 12 23:42:26 2015

@author: Artem Sevastopolsky
"""

LAMBDA = 0 

import numpy as np
from featureVector import FEATURES_NUM

# Sigmoid function
g = lambda a: 1.0 / (1.0 + np.exp(-a))  # works both for vectors and matrices

# Cost function
# X is a train set
# J(theta) = -1/m * (np.dot(log(g(np.dot(X,theta)))', y) 
#                   + np.dot(log(1 - g(np.dot(X,theta)))', 1-y))
#                   + LAMBDA / 2m * sum{k=1..n}(theta[k]**2)
# multiplier deleted
cost = lambda theta, X, y: ((-np.dot(np.transpose(np.log(g(np.dot(X, theta)))), y) - \
        np.dot(np.transpose(np.log(1 - g(np.dot(X, theta)))), 1-y))[0, 0] + \
        LAMBDA / 2.0 * np.sum(theta[1:] * theta[1:]))

def gradDescent(X, y):
    # X is a train set
    ALPHA = 0.8
    EPS = 1e-7
    MAX_ITER = 500
    
    trainVol = X.shape[0]
    theta = np.zeros((FEATURES_NUM, 1))
    Jprev = None
    Jlast = cost(theta, X, y)
    iterNum = 0
    #J = []  # for debug
    while iterNum < MAX_ITER and (Jprev == None or abs(Jprev - Jlast) > EPS):
        thetaCleared = np.copy(theta)
        thetaCleared[0] = 0
        theta -= ALPHA / trainVol * \
            (np.dot(X.transpose(), g(np.dot(X, theta)) - y) \
            + LAMBDA * thetaCleared)
        Jprev = Jlast
        Jlast = cost(theta, X, y)
        #J.append(Jlast)
        iterNum += 1
    #plt.plot(J, label="J of iterations")
    return theta

def prediction(testSet, theta):
    return g(np.dot(testSet, theta))