# -*- coding: utf-8 -*-
"""
Created on Thu Feb 26 11:40:53 2015

@author: Artem S
"""

from datetime import datetime
from featureVector import FEATURES_NUM

INPUT_DIR = r"C:\Users\Maria1967\Documents\Kaggle Drivers\drivers\drivers"
OUTPUT_DIR = r"C:\Users\Maria1967\Documents\Kaggle Drivers\solutions"
FEATURES_DIR = r"C:\Users\Maria1967\Documents\Kaggle Drivers\features4q"
MAX_TRIPS = 200

import os
import csv
import numpy as np

def driverExists(driverNo):
    filePath = os.path.join(INPUT_DIR, "{}".format(driverNo), "1.csv")
    return os.path.exists(filePath)

def getAvailableDrivers():
    folderNames = os.listdir(INPUT_DIR)
    drivers = map(int, folderNames)
    drivers.sort()
    return drivers
    
def openWriteStream(mode="report", *args):
    # Returns file object and _csv.writer object
    # Acceptable modes: "report", "features", "none"
    # In "features" mode, specify driverNo in args
    # In "none" mode, specify filename in args
    
    if mode == "report":
        filePath = datetime.now().strftime("%d-%m-%Y %H-%M-%S") + \
            "({}).csv".format(args[0])
        filePath = os.path.join(OUTPUT_DIR, filePath)
    elif mode == "features":
        filePath = os.path.join(FEATURES_DIR, "{}.csv".format(args[0]))
    elif mode == "none":
        filePath = args[0]
    else:
        return (None, None)
    
    csvFile = open(filePath, 'wb')
    csvWriter = csv.writer(csvFile)
    if mode == "report":
        csvWriter.writerow(("driver_trip", "prob"))
    
    return (csvFile, csvWriter)

def openReadStream(mode="none", *args):
    # Possible modes:
    # "none" => filename in args
    # "report" => filename in args
    # "features" => driverNo in args
    # "input" => driverNo, tripNo in args
    
    if mode == "none" or mode == "report":
        filePath = args[0]  # must be a full path
        if mode == "report":
            os.chdir(OUTPUT_DIR)
    elif mode == "features":
        driverNo = args[0]
        filePath = os.path.join(FEATURES_DIR, "{}.csv".format(driverNo))
    elif mode == "input":
        driverNo, tripNo = args
        filePath = os.path.join(INPUT_DIR, '{}'.format(driverNo), '{}.csv'.format(tripNo))
    
    csvFile = open(filePath, 'rb')
    csvReader = csv.reader(csvFile)
    return (csvFile, csvReader)

def closeStream(csvFile):
    print "File {} written".format(csvFile.name)
    csvFile.close()

# --------------
# More upper-level functions
# --------------

def inputRead(driverNo, tripNo):
    pnts = []
    csvFile, csvReader = openReadStream("input", driverNo, tripNo)
    LINES_MAX_TO_READ = 10000
    lastLine = ""
    i = 0
    while i < LINES_MAX_TO_READ:
        try:
            lastLine = csvReader.next()
            if i > 0:   # skipping "x, y" line
                pnts.append(map(float, lastLine))
        except StopIteration:
            break
        i += 1
    csvFile.close()
    return pnts

def reportForDriver(filename, driverNo):
    filePath = os.path.join(OUTPUT_DIR, filename)
    csvFile, csvReader = openReadStream("none", filePath)
    LINES_MAX_TO_READ = 600000
    
    strDriverNo = str(driverNo)
    ans = np.empty(MAX_TRIPS + 1)   # row 0 - fictive
    i = tripsFound = 1
    csvReader.next()    # skipping "driver_trip,prob" line
    while i < LINES_MAX_TO_READ:
        try:
            line = csvReader.next()
        except StopIteration:
            break
        else:
            line[0] = line[0].split('_')
            if line[0][0] == strDriverNo:
                ans[tripsFound] = float(line[1])
                tripsFound += 1
            i += 1
    
    csvFile.close()
    return ans

def readFirstFeatures(driverNo, lines=MAX_TRIPS):
    csvFile, csvReader = openReadStream("features", driverNo)
    ans = np.empty((lines, FEATURES_NUM))
    for i in xrange(lines):
        try:
            line = csvReader.next()
        except StopIteration:
            break
        ans[i, :] = np.array(map(float, line))
    csvFile.close()
    return ans

def getFeatures(csvReader, driverNo, tripNums):
    tripNums.sort()
    csvFile, csvReader = openReadStream("features", driverNo)
    ans = np.empty(tripNums, FEATURES_NUM)
    tripIdx = 0
    for i in xrange(1, MAX_TRIPS + 1):
        try:
            line = csvReader.next()
        except StopIteration:
            break
        if i == tripNums[tripIdx]:
            ans[tripIdx, :] = np.array(map(float, line))
            tripIdx += 1
    csvFile.close()
    return ans

def reportWrite(csvWriter, driverNo, driverAnswer):
    for tripNo in xrange(1, driverAnswer.size):
        csvWriter.writerow(("{}_{}".format(driverNo, tripNo), driverAnswer[tripNo]))