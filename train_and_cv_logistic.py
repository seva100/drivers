# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 21:18:18 2015

@author: Artem S
"""

'''
Driver 1 - repeated:
[129, 139, 154, 160, 17, 170, 180, 182, 31, 33, 43, 46, 58, 60, 68, 86, 95]
 17 repeated trips
Driver 2 - repeated:
[100, 101, 103, 106, 108, 115, 12, 121, 128, 136, 138, 157, 159, 171, 173, 178, 
186, 197, 199, 26, 42, 50, 59, 61, 68, 70, 79, 81, 83, 87, 88, 89, 95]
Driver 3 - repeated:
[117, 129, 130, 137, 171, 185, 198, 39, 42, 70, 74, 81, 85]
Driver 10 - repeated:
[104, 109, 110, 115, 121, 134, 139, 140, 146, 152, 163, 166, 183, 188, 197, 20, 31, 41, 46, 47, 55, 56, 6, 62, 74, 82, 83, 97]
Driver 11 - repeated:
[108, 109, 111, 113, 119, 122, 127, 128, 132, 135, 138, 140, 141, 143, 146, 149, 15, 156, 164, 165, 166, 168, 169, 171, 176, 177, 178, 179, 18, 185, 188, 190, 195, 197, 198, 20, 21, 25, 26, 30, 33, 34, 35, 38, 39, 4, 41, 45, 48, 5, 54, 58, 59, 6, 61, 63, 7, 70, 71, 72, 77, 81, 82, 91, 98]

'''

MAX_DRIVERS = 3612
#MAX_DRIVERS = 1000
DRIVERS_TO_CHECK = 11
MAX_TRIPS = 200
CV_VOL_FALSE = 250
CV_VOL_PIECE = 25 

TRAIN_VOL_TRUE = 200
TRAIN_VOL_FALSE = 800
TRAIN_VOL_PIECE = 25
TRAIN_VOL = TRAIN_VOL_TRUE + TRAIN_VOL_FALSE

import random
from math import ceil
import numpy as np
import sklearn.metrics
import csvHandle
from logistic_regression import gradDescent, prediction
from featureVector import FEATURES_NUM

updiv = lambda a, b: int(ceil(float(a) / b))

def normalizeSet(X):
    X[:, 1:] -= X[:, 1:].mean(axis=0)
    Xdiv = X[:, 1:].std(axis=0)
    Xdiv[np.isclose(Xdiv, 0)] = 1
    X[:, 1:] /= Xdiv
    return X

def makeTrainSet(driverNo):
    trainSet = np.empty((TRAIN_VOL, FEATURES_NUM))
    
    trainTrips = range(1, TRAIN_VOL_TRUE + 1)
    # we'll choose the first TRAIN_VOL_TRUE trips of current driver
    trainSet[:TRAIN_VOL_TRUE, :] = csvHandle.readFirstFeatures(driverNo, \
        lines=TRAIN_VOL_TRUE)
    
    # We choose the first trips from each driver that's different from ours
    falseTripsChosen = 0
    chosenDrivers = np.array(csvHandle.getAvailableDrivers())
    chosenDrivers = chosenDrivers[chosenDrivers != driverNo]
    chosenDrivers = chosenDrivers[chosenDrivers <= MAX_DRIVERS]
    random.shuffle(chosenDrivers)
    chosenDrivers = chosenDrivers[:updiv(TRAIN_VOL_FALSE, TRAIN_VOL_PIECE)]
    for diffDriver in chosenDrivers:
        if falseTripsChosen >= TRAIN_VOL_FALSE:
            break
        toChoose = min(MAX_TRIPS, 
                       TRAIN_VOL_FALSE - falseTripsChosen, 
                       TRAIN_VOL_PIECE)
        if not csvHandle.driverExists(diffDriver):
            continue
        trainSet[(TRAIN_VOL_TRUE + falseTripsChosen):\
            (TRAIN_VOL_TRUE + falseTripsChosen + toChoose), :] = \
            csvHandle.readFirstFeatures(diffDriver, lines=toChoose)
        falseTripsChosen += toChoose
    
    return (trainSet, trainTrips)

def dictPPrint(d):
    for k, v in sorted(d.iteritems()):
        print "{:<8} {:<15}".format(k, v)

if __name__ == "__main__":
    assert TRAIN_VOL_TRUE <= MAX_TRIPS    
    
    DRIVERS_LIST = np.array(csvHandle.getAvailableDrivers())
    DRIVERS_LIST = DRIVERS_LIST[DRIVERS_LIST <= DRIVERS_TO_CHECK]
    aucStats = {}
    for driverNo in DRIVERS_LIST:
        if driverNo == 1:
            PROVED_TRUE = (129, 139, 154, 160, 17, 170, 180, 182, 31, 33, 43, 46, 58, 60, 68, 86, 95)
        elif driverNo == 2:
            PROVED_TRUE = (100, 101, 103, 106, 108, 115, 12, 121, 128, 136, 138, 157, 159, 171, 173, 178, 186, 197, 199, 26, 42, 50, 59, 61, 68, 70, 79, 81, 83, 87, 88, 89, 95)
        elif driverNo == 3:
            PROVED_TRUE = (117, 129, 130, 137, 171, 185, 198, 39, 42, 70, 74, 81, 85)
        elif driverNo == 10:
            PROVED_TRUE = (104, 109, 110, 115, 121, 134, 139, 140, 146, 152, 163, 166, 183, 188, 197, 20, 31, 41, 46, 47, 55, 56, 6, 62, 74, 82, 83, 97)
        elif driverNo == 11:
            PROVED_TRUE = (108, 109, 111, 113, 119, 122, 127, 128, 132, 135, 138, 140, 141, 143, 146, 149, 15, 156, 164, 165, 166, 168, 169, 171, 176, 177, 178, 179, 18, 185, 188, 190, 195, 197, 198, 20, 21, 25, 26, 30, 33, 34, 35, 38, 39, 4, 41, 45, 48, 5, 54, 58, 59, 6, 61, 63, 7, 70, 71, 72, 77, 81, 82, 91, 98)
        
        CV_VOL_TRUE = len(PROVED_TRUE)
        CV_VOL = CV_VOL_TRUE + CV_VOL_FALSE
        if not csvHandle.driverExists(driverNo):
            print "unable to work with driver #{}".format(driverNo)
            continue
        trainSet, trainTrips = makeTrainSet(driverNo)
        trainSet = normalizeSet(trainSet)
        
        y = np.ones((TRAIN_VOL, 1))
        y[TRAIN_VOL_TRUE:, :] = 0
        
        theta = gradDescent(trainSet, y)
        
        cvSet = np.empty((CV_VOL, FEATURES_NUM))
        allFeatures = csvHandle.readFirstFeatures(driverNo)     # reads all
        for i in xrange(len(PROVED_TRUE)):
            cvSet[i, :] = allFeatures[PROVED_TRUE[i] - 1]
        
        # We choose the first trips from each driver that's different from ours
        falseTripsChosen = 0
        chosenDrivers = np.array(csvHandle.getAvailableDrivers())
        chosenDrivers = chosenDrivers[chosenDrivers != driverNo]
        chosenDrivers = chosenDrivers[chosenDrivers <= MAX_DRIVERS]
        random.shuffle(chosenDrivers)
        chosenDrivers = chosenDrivers[:updiv(CV_VOL_FALSE, CV_VOL_PIECE)]
        for diffDriver in chosenDrivers:
            if falseTripsChosen >= CV_VOL_FALSE:
                break
            toChoose = min(MAX_TRIPS, 
                           CV_VOL_FALSE - falseTripsChosen, 
                           CV_VOL_PIECE)
            if not csvHandle.driverExists(diffDriver):
                continue
            cvSet[(CV_VOL_TRUE + falseTripsChosen):\
                (CV_VOL_TRUE + falseTripsChosen + toChoose), :] = \
                csvHandle.readFirstFeatures(diffDriver, lines=toChoose)
            falseTripsChosen += toChoose
            
        cvSet = normalizeSet(cvSet)
        
        hypothesis = prediction(cvSet, theta)
        hypothesis = hypothesis.reshape(hypothesis.size)
        label = np.ones(CV_VOL)
        label[CV_VOL_TRUE:] = 0

        fpr, tpr, thresholds = sklearn.metrics.roc_curve(label, hypothesis, pos_label=1)
        auc = sklearn.metrics.auc(fpr, tpr)  
        print("Score for driver {}".format(driverNo))
        aucStats[driverNo] = auc
        print(auc)
    
    dictPPrint(aucStats)