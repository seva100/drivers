# -*- coding: utf-8 -*-
"""
Created on Sat Feb 28 13:16:30 2015

@author: Artem S
"""

MAX_DRIVERS = 3612
#MAX_DRIVERS = 20
MAX_TRIPS = 200
TRAIN_VOL_TRUE = 200
TRAIN_VOL_FALSE = 800
TRAIN_VOL_PIECE = 25
TRAIN_VOL = TRAIN_VOL_TRUE + TRAIN_VOL_FALSE
LAMBDA = 0                                  # regularization parameter

from math import ceil
import numpy as np
#import matplotlib.pyplot as plt
import random
#from multiprocessing import Pool

import csvHandle
from featureVector import FEATURES_NUM
from logistic_regression import gradDescent, prediction

updiv = lambda a, b: int(ceil(float(a) / b))

def normalizeSet(X):
    X[:, 1:] -= X[:, 1:].mean(axis=0)
    Xdiv = X[:, 1:].std(axis=0)
    Xdiv[np.isclose(Xdiv, 0)] = 1
    X[:, 1:] /= Xdiv
    return X

def makeTrainSet(driverNo):
    trainSet = np.empty((TRAIN_VOL, FEATURES_NUM))
    
    trainTrips = range(1, TRAIN_VOL_TRUE + 1)
    # we'll choose the first TRAIN_VOL_TRUE trips of current driver
    trainSet[:TRAIN_VOL_TRUE, :] = csvHandle.readFirstFeatures(driverNo, \
        lines=TRAIN_VOL_TRUE)
    
    # We choose the first trips from each driver that's different from ours
    falseTripsChosen = 0
    chosenDrivers = np.array(csvHandle.getAvailableDrivers())
    chosenDrivers = chosenDrivers[chosenDrivers != driverNo]
    chosenDrivers = chosenDrivers[chosenDrivers <= MAX_DRIVERS]
    random.shuffle(chosenDrivers)
    chosenDrivers = chosenDrivers[:updiv(TRAIN_VOL_FALSE, TRAIN_VOL_PIECE)]
    for diffDriver in chosenDrivers:
        if falseTripsChosen >= TRAIN_VOL_FALSE:
            break
        toChoose = min(MAX_TRIPS, 
                       TRAIN_VOL_FALSE - falseTripsChosen, 
                       TRAIN_VOL_PIECE)
        if not csvHandle.driverExists(diffDriver):
            continue
        #for i in xrange(toChoose):
        #    pnts = np.array(csvHandle.csvRead(diffDriver, i + 1))
        #    trainSet[TRAIN_VOL_TRUE + falseTripsChosen + i, :] = featureVector(pnts)
        trainSet[(TRAIN_VOL_TRUE + falseTripsChosen):\
            (TRAIN_VOL_TRUE + falseTripsChosen + toChoose), :] = \
            csvHandle.readFirstFeatures(diffDriver, lines=toChoose)
        falseTripsChosen += toChoose
    
    return (trainSet, trainTrips)

if __name__ == "__main__":
    assert TRAIN_VOL_TRUE <= MAX_TRIPS
    
    csvFile, csvWriter = csvHandle.openWriteStream("report", "logistic3")
    
    DRIVERS_LIST = np.array(csvHandle.getAvailableDrivers())
    DRIVERS_LIST = DRIVERS_LIST[DRIVERS_LIST <= MAX_DRIVERS]
    hypothesis = np.empty(MAX_TRIPS + 1)
    for driverNo in DRIVERS_LIST:   
        if not csvHandle.driverExists(driverNo):
            print "unable to work with driver #{}".format(driverNo)
            continue
        trainSet, trainTrips = makeTrainSet(driverNo)
        trainSet = normalizeSet(trainSet)
        y = np.ones((TRAIN_VOL, 1))
        y[TRAIN_VOL_TRUE:, :] = 0
        
        theta = gradDescent(trainSet, y)
        
        testSet = csvHandle.readFirstFeatures(driverNo)     # reads all
        testSet = normalizeSet(testSet)
        
        DECISION_BOUNDARY_LVL = 0.1     # just for debug purposes
        hypothesis[1:] = prediction(testSet, theta).reshape(MAX_TRIPS)
        falseExamples = np.where(hypothesis < DECISION_BOUNDARY_LVL)[0]
        csvHandle.reportWrite(csvWriter, driverNo, hypothesis)
        print("prepared answer for driver #{}, num of false examples = {}".format(\
            driverNo, falseExamples.size))
    
    csvHandle.closeStream(csvFile)