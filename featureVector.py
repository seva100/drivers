# -*- coding: utf-8 -*-
"""
Created on Thu Feb 26 21:51:45 2015

@author: Artem S
"""

CENTRIPETAL_FR = (float('-inf'), -6.0, -4.0, -2.0, -1.5, -1.0, -0.5, -0.2,)
CENTRIPETAL_FR += tuple(-el for el in CENTRIPETAL_FR)[::-1]
VEL_FR = (0.0, 0.1, 0.2, 0.5, 2.0, 5.0, 8.0, 15.0, 25.0, 40.0, float('inf'))
ACC_FR = (0.0, 0.1, 0.2, 0.4, 0.6, 0.8, 1.0, 1.5, 2.0, 4.0, float('inf'))
SLOWDN_FR = tuple(-el for el in ACC_FR)[::-1]

import numpy as np
import matplotlib.pyplot as plt

def hits(x, thresholds):
    # x is expected to be np.array (vector), thresholds - array_like
    # thresholds must be already sorted
    # it's ok to add float('inf') at the end of thresholds
    ans = []
    for i in xrange(len(thresholds) - 1):
        ans.append(np.sum((thresholds[i] < x) & (x < thresholds[i + 1])) / \
            float(len(x)))
    return ans

def featureVector(tripCoord):
    '''tripCoord np vector -> featureVr np vector
    featureVr components:
    - equals to 1 (for logistic regression)
    - quantiles for centripetal acceleration
    - quantiles for speed
    - quantiles for acceleration
    - quantiles for slowdown
    '''
    featureVr = [1]
    heading = tripCoord[1:, :] - tripCoord[:-1, :]
    dist = np.sqrt(np.sum(heading * heading, axis=1))
    angle = np.arctan2(heading[1:, 1], heading[1:, 0]) - \
        np.arctan2(heading[:-1, 1], heading[:-1, 0])
    centripetalAccel = angle[1:] - angle[:-1]
    #plt.hist(centripetalAccel, bins=100)
    
    featureVr.extend(hits(centripetalAccel, CENTRIPETAL_FR))
    
    featureVr.extend(hits(dist, VEL_FR))
    
    #TODO it'd be nice to count acceleration not for every pair of seconds,
    # but for larger time.
    accel = (dist[1:] - dist[:-1]) / 2.0
    featureVr.extend(hits(accel, ACC_FR))
    featureVr.extend(hits(accel, SLOWDN_FR))
    '''
    # squared features
    for i in xrange(1, BASIC_FEATURES):
        for j in xrange(i, BASIC_FEATURES):
            featureVr = np.append(featureVr, featureVr[i] * featureVr[j])
    
    '''
    return featureVr

# should remain the same for the same implementation of featureVector()
FEATURES_NUM = len(featureVector(np.array(np.zeros((50, 2)))))
