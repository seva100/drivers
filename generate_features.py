# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 13:13:17 2015

@author: Artem S
"""

MAX_DRIVERS = 3612
#MAX_DRIVERS = 20
MAX_TRIPS = 200

import csvHandle
import numpy as np
from featureVector import featureVector

if __name__ == "__main__": 
    DRIVERS_LIST = np.array(csvHandle.getAvailableDrivers())
    DRIVERS_LIST = DRIVERS_LIST[DRIVERS_LIST <= MAX_DRIVERS] 
    for driverNo in DRIVERS_LIST:
        if not csvHandle.driverExists(driverNo):
            print "unable to work with driver {}".format(driverNo)
            continue
        csvFile, csvWriter = csvHandle.openWriteStream("features", driverNo)
        for tripNo in xrange(1, MAX_TRIPS + 1):
            pnts = csvHandle.inputRead(driverNo, tripNo)
            pnts = np.array(pnts)
            fVr = featureVector(pnts)
            csvWriter.writerow(fVr)
        print "features for driver {} stored".format(driverNo)
        csvFile.close()